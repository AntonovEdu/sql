CREATE TABLE person
(
    person_id BIGINT AUTO_INCREMENT PRIMARY KEY
   ,fname VARCHAR(100) NOT NULL
   ,name VARCHAR(100) NOT NULL
   ,sname VARCHAR(100) NOT NULL
   ,when_born DATE NOT NULL
   ,sex TINYINT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE job
(
    job_id BIGINT AUTO_INCREMENT PRIMARY KEY
   ,job_name VARCHAR(100) NOT NULL
   ,UNIQUE KEY (`job_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE department_type
(
     department_type_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,department_type VARCHAR(500) NOT NULL
    ,UNIQUE KEY (`department_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE department
(
     department_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,department_full_name VARCHAR(500) NOT NULL
    ,department_short_name CHAR(15) NOT NULL
    ,department_type_id BIGINT NOT NULL
    ,parent_department_id BIGINT NULL
    ,UNIQUE KEY (`department_full_name`)
    ,FOREIGN KEY (`parent_department_id`) REFERENCES `department`(`department_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
    ,FOREIGN KEY (`department_type_id`) REFERENCES `department_type`(`department_type_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE employee
(
     employee_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,person_id BIGINT NOT NULL
    ,job_id BIGINT NOT NULL
    ,department_id BIGINT NOT NULL
    ,start_date  DATE   NOT NULL
    ,end_date    DATE   NULL
    ,FOREIGN KEY (`person_id`) REFERENCES `person`(`person_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
    ,FOREIGN KEY (`job_id`) REFERENCES `job`(`job_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
    ,FOREIGN KEY (`department_id`) REFERENCES `department`(`department_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE speciality
(
     speciality_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,speciality_full VARCHAR(100)  NOT NULL
    ,specialiti_short CHAR(10) NOT NULL
    ,UNIQUE KEY (`speciality_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE education_form
(
     education_form_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,education_form_full VARCHAR(100) NOT NULL
    ,education_form_short CHAR(10) NOT NULL
    ,UNIQUE KEY (`education_form_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE education_level
(
     education_level_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,education_level_full VARCHAR(100) NOT NULL
    ,education_level_short CHAR(10) NOT NULL
    ,UNIQUE KEY (`education_level_full`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE group_flow
(
    group_flow_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,speciality_id BIGINT NOT NULL
    ,education_level_id BIGINT NOT NULL
    ,education_form_id BIGINT NOT NULL # ??
    ,year INT NOT NULL
    ,FOREIGN KEY (`speciality_id`) REFERENCES `speciality`(`speciality_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
    ,FOREIGN KEY (`education_level_id`) REFERENCES `education_level`(`education_level_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
    ,FOREIGN KEY (`education_form_id`) REFERENCES `education_form`(`education_form_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
            
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE groups
(
     groups_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,group_flow_id BIGINT NOT NULL
    ,number BIGINT NOT NULL
    ,FOREIGN KEY (`group_flow_id`) REFERENCES `group_flow`(`group_flow_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE students
(
     students_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,person_id BIGINT  NOT NULL
    ,groups_id BIGINT NOT NULL
    ,isBudget TINYINT NOT NULL
    ,FOREIGN KEY (`person_id`) REFERENCES `person`(`person_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
    ,FOREIGN KEY (`groups_id`) REFERENCES `groups`(`groups_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE district
(
     district_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,district VARCHAR(100)  NOT NULL
    ,UNIQUE KEY (`district`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE city
(
     city_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,district_id BIGINT  NOT NULL
    ,city VARCHAR(100) NOT NULL
    ,FOREIGN KEY (`district_id`) REFERENCES `district`(`district_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE street
(
     street_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,street VARCHAR(150)  NOT NULL
    ,city_id BIGINT NOT NULL
    ,FOREIGN KEY (`city_id`) REFERENCES `city`(`city_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE contact_type
(
     contact_type_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,contact_type VARCHAR(100)  NOT NULL
    ,UNIQUE KEY (`contact_type`)
    
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE person_contact
(
     city_id BIGINT AUTO_INCREMENT PRIMARY KEY
    ,contact_type_id BIGINT  NOT NULL
    ,person_id BIGINT  NOT NULL
    ,contact VARCHAR(100) NOT NULL
    ,FOREIGN KEY (`contact_type_id`) REFERENCES `contact_type`(`contact_type_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
    ,FOREIGN KEY (`person_id`) REFERENCES `person`(`person_id`)
            ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

