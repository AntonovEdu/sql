INSERT INTO person(person_id, fname, name, sname, when_born, sex)
VALUES
(1,'Дамблдор','Альбус','-','1980-01-17',1),
(2,'Кожем\'яка','Микита','Семенович','2005-12-15',1),
(3,'Organa Solo','Leia ','-','1979-12-12',0);

INSERT INTO job(job_id, job_name)
VALUES
(1,"Ректор"),
(2,"Викладач"),
(3,"Старший викладач"),
(4,"Доцент"),
(5,"Професор");

INSERT INTO department_type (department_type_id, department_type)
VALUES
(1,"Ректорат"),
(2,"Вчена рада"),
(3,"Факультет"),
(4,"Деканат"),
(5,"Лабораторія"),
(6,"Кафедра");


INSERT INTO department (department_id, department_full_name, department_short_name, department_type_id, parent_department_id)
VALUES
(1,"Ректорат ДонНУ","Рек.ДонНУ",1,NULL),
(2,"Математичний","Матфак",3,1),
(3,"Фізичний","Фізфак",3,1),
(4,"Прикладної математики","ПМ",6,2),
(5,"Теорії пружності та обчислювальної математики","ТПтаОМ",6,2),
(6,"Прикладної фізики","ПФ",6,3);

INSERT INTO employee (employee_id, person_id, job_id, department_id, start_date, end_date)
VALUES
(1, 1, 1, 1, "1999-01-01",NULL);
     
INSERT INTO speciality(speciality_id, speciality_full, specialiti_short)
VALUES
(1,"Прикладна математика","ПМ");

INSERT INTO education_form (education_form_id, education_form_full, education_form_short)
VALUES
(1,"Денна","д"),
(2,"Заочна","з");

INSERT INTO education_level (education_level_id, education_level_full, education_level_short)
VALUES
(1, "Бакалавр", "Б"),
(2, "Магістр", "М"),
(3, "Спеціаліст", "С");

INSERT INTO group_flow (group_flow_id, speciality_id, education_level_id, education_form_id, year)
VALUES
(1, 1, 1, 1, 2012);

INSERT INTO groups (groups_id, group_flow_id, number)
VALUES
(1, 1, 1),
(2, 1, 2);

INSERT INTO students (students_id, person_id, groups_id, isBudget)
VALUES
(1, 2, 1, 1),
(2, 3, 2, 1);

INSERT INTO district (district_id, district)
VALUES
(1, "Вінницька"),
(2, "Донецька"),
(3, "Дніпорпетровська");

INSERT INTO city (city_id, district_id, city)
VALUES
(1, 1, "Вінниця"),
(2, 2, "Донецьк"),
(3, 3, "Дніпорпетровськ"),
(4, 3, "Дніпро");

INSERT INTO street (street_id, street, city_id)
VALUES
(1, "Київська", 1),
(2, "Київська", 2);

INSERT INTO contact_type (contact_type_id, contact_type)
VALUES
(1, "Телефон"),
(2, "email");



INSERT INTO person_contact (city_id, contact_type_id, person_id, contact)
VALUES
(1, 1, 1, "+987-777-987-65-55"),
(2, 2, 1, "dambledor@hogvards.net");

